<?php
/**
 * Created by PhpStorm.
 * User: Nil
 * Date: 29/11/2017
 * Time: 10:52
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Brand;
use AppBundle\Entity\Phone;
use AppBundle\Entity\User;
use AppBundle\Form\Type\BrandType;
use AppBundle\Form\Type\ChangePasswordType;
use AppBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdminController extends Controller
{
    /**
     * @Route("/admin")
     */
    public function indexAction(){
        return $this->render('Admin/admin_home.html.twig');
    }

    /**
     * @Route("/admin/password")
     */
    public function passwordAction(Request $request){
        $user = $this->getUser();

        $form = $this->createForm(ChangePasswordType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $encoder = $this->get('security.password_encoder');
            $encode_password = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($encode_password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_admin_password');
        }

        return $this->render('User/change_user_password.html.twig', [
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/user/")
     */
    public function usersListAction(Request $request){
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        //Retourne la liste des annonces
        return $this->render('Admin/users_list.html.twig', [
            'users'=>$users
        ]);
    }

    /**
     * @Route("/admin/user/create")
     */
    public function createUserAction(Request $request){
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $encoder = $this->get('security.password_encoder');
            $encode_password = $encoder->encodePassword($user, $user->getPassword());
            $user->setRole("user");
            $user->setPassword($encode_password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_admin_createuser');
        }

        return $this->render('user/create_user.html.twig', [
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/brand")
     */
    public function brandsListAction(){
        $brands = $this->getDoctrine()
            ->getRepository(Brand::class)
            ->findAll();

        //Retourne la liste des annonces
        return $this->render('Admin/brands_list.html.twig', [
            'brands'=>$brands
        ]);
    }

    /**
     * @Route("/admin/brand/create")
     */
    public function createBrandAction(Request $request){
        $brand = new Brand();

        $form = $this->createForm(BrandType::class, $brand);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($brand);
            $em->flush();

            return $this->redirectToRoute('app_admin_brandslist');
        }

        $brands = $this->getDoctrine()
            ->getRepository(Brand::class)
            ->findAll();

        return $this->render('Admin/create_brand.html.twig', [
            'brandForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/brand/{id}")
     */
    public function updateBrandAction($id, Request $request){
        $brand = $this->getDoctrine()
            ->getRepository(Brand::class)
            ->find($id);

        $form = $this->createForm(BrandType::class, $brand);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($brand);
            $em->flush();

            return $this->redirectToRoute('app_admin_createbrand');
        }

        return $this->render('Admin/create_brand.html.twig', [
            'brandForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/brand/{id}/delete")
     */
    public function deleteBrandAction($id){
        //récupération de la marque concernée
        $brand = $this->getDoctrine()
            ->getRepository(Brand::class)
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($brand);
        $em->flush();

        $brands = $this->getDoctrine()
            ->getRepository(Brand::class)
            ->findAll();

        return $this->render('Admin/brands_list.html.twig', [
            'message' => 'La marque a bien été supprimée.',
            'brands' => $brands
        ]);
    }

    /**
     * @Route("/admin/user/{id}")
     */
    public function updateUserAction($id, Request $request){
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_admin_updateuser', [
                'id' => $id
            ]);
        }

        return $this->render('Admin/update_user.html.twig', [
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/user/{id}/delete")
     */
    public function deleteUserAction($id){
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);

        //On ne peut pas supprimer les autres admins
        if($user->getRole() == 'admin'){
            throw $this->createNotFoundException('Oops ! Page not found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        return $this->render('Admin/users_list.html.twig', [
            'message' => "L'utilisateur a bien été supprimé",
            'users' => $users,
        ]);
    }

    /**
     * @Route("/admin/phone")
     */
    public function phonesListAction(){
        $phones = $this->getDoctrine()
            ->getRepository(Phone::class)
            ->findAll();

        //Retourne la liste des annonces
        return $this->render('Admin/phones_lists.html.twig', [
            'phones'=>$phones
        ]);
    }
}