<?php
/**
 * Created by PhpStorm.
 * User: Nil
 * Date: 27/11/2017
 * Time: 16:08
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Form\Type\ChangePasswordType;
use AppBundle\Form\Type\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route("/register")
     * @Method("GET|POST")
     */
    public function createUserAction(Request $request)
    {
        if($this->getUser() != null){
            return $this->redirectToRoute('app_announce_index');
        }

        $user = new User();

        $actualUser = $this->getUser();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $encoder = $this->get('security.password_encoder');
            $encode_password = $encoder->encodePassword($user, $user->getPassword());
            $user->setRole("user");
            $user->setPassword($encode_password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_announce_announcelist');
        }

        return $this->render('user/create_user.html.twig', [
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user")
     */
    public function indexAction(){
        $user = $this->getUser();

        return $this->render('User/user_home.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/user/account")
     */
    public function accountAction(Request $request){

        $user = $this->getUser();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_user_account');
        }

        return $this->render('User/create_user.html.twig', [
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/password")
     */
    public function passwordAction(Request $request){
        $user = $this->getUser();

        $form = $this->createForm(ChangePasswordType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $encoder = $this->get('security.password_encoder');
            $encode_password = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($encode_password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_user_password');
        }

        return $this->render('User/change_user_password.html.twig', [
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("user/phone")
     */
    public function myAnnounceListAction(){
        $phones = $this->getUser()->getPhones();

        return $this->render('Announce/announces.html.twig', [
            'phones' => $phones,
            'title_announce' => "Mes annonces",
        ]);

    }
}