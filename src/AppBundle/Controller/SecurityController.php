<?php
/**
 * Created by PhpStorm.
 * User: Nil
 * Date: 29/11/2017
 * Time: 10:24
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Phone;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends Controller
{
    /**
     * @Route("/connexion")
     */
    public function indexAction(Request $request){

        if($this->getUser() != null){
            return $this->redirectToRoute('app_announce_index');
        }

        $authenticationUtils = $this->get('security.authentication_utils');

        return $this->render('security/login.html.twig', [
            'lastUsername' => $authenticationUtils->getLastUsername(),
            'error' => $authenticationUtils->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/succes")
     */
    public function targetAction(){

        $phones = $this->getDoctrine()
            ->getRepository(Phone::class)
            ->findAll();

        return $this->render('Announce/index.html.twig', [
            'message' => 'Connexion réussie',
            'phones' => $phones
        ]);
    }
}