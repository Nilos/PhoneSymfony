<?php
/**
 * Created by PhpStorm.
 * User: Nil
 * Date: 27/11/2017
 * Time: 16:21
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Phone;
use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use AppBundle\Form\Type\CreatePhotoType;
use AppBundle\Form\Type\PhoneType;
use AppBundle\Form\Type\SearchAnnouncesType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class AnnounceController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $phones = $this->getDoctrine()
            ->getRepository(Phone::class)
            ->findAll();


        $phones = $this->getDoctrine()
            ->getRepository(Phone::class)
            ->findLastPhones();

        //Retourne la vue de la page d'accueil
        return $this->render('Announce/index.html.twig', [
            'phones'=>$phones
        ]);
    }

    /**
     * @Route("/annonces")
     * @Method("GET|POST")
     */
    public function announceListAction(Request $request)
    {
        $form = $this->createForm(SearchAnnouncesType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $data = $form->getData();

            $repository = $this->getDoctrine()->getRepository(Phone::class);

            $phones = $this->getDoctrine()
                ->getRepository(Phone::class)
                ->findWhereLike($data['search']);

            //Retourne la liste des annonces
            return $this->render('Announce/announces.html.twig', [
                'phoneForm' => $form->createView(),
                'phones'=>$phones
            ]);
        }

        $phones = $this->getDoctrine()
            ->getRepository(Phone::class)
            ->findAll();

        //Retourne la liste des annonces
        return $this->render('Announce/announces.html.twig', [
            'phoneForm' => $form->createView(),
            'phones'=>$phones
        ]);
    }

    /**
     * @Route("/annonces/{id}")
     */
    public function announceAction($id){

        $phone = $this->getDoctrine()->getRepository(Phone::class)->find($id);

        //Retourne une annonce en particulier
        return $this->render('Announce/details_product.html.twig', [
            'phone'=>$phone
        ]);
    }

    /**
     * @Route("/user/phone/create")
     * @Route("/admin/phone/create")
     * @Method("GET|POST")
     */
    public function createAnnounceAction(Request $request)
    {

        $phone = new Phone();

        $form = $this->createForm(PhoneType::class, $phone);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $phone->setCreated(new \DateTime());
            $phone->setUpdated(new \DateTime());

            $user = $this->getUser();

            $phone->setUser($user);
            $user->addPhone($phone);

            $em = $this->getDoctrine()->getManager();
            $em->persist($phone);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_announce_announcelist');
        }

        return $this->render('User/create_phone.html.twig', [
            'phoneForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user/phone/{id}/photo/create")
     * @Route("/admin/phone/{id}/photo/create")
     */
    public function addPhotoAnnounceAction($id, Request $request){

        //récupération du téléphone concerné
        $phone = $this->getDoctrine()
            ->getRepository(Phone::class)
            ->find($id);

        if(!$this->getUser()->isAdmin() and $phone->getUser()->getId() != $this->getUser()->getId()){
            throw $this->createNotFoundException('Oops ! Page not found');
        }

        $photos = $phone->getPhotos();

        $photo = new Photo();
        $form = $this->createForm(CreatePhotoType::class, $photo);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $phone->addPhoto($photo);
            $photo->setPhone($phone);

            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);

            $em->persist($phone);
            $em->flush();

            return $this->redirectToRoute('app_announce_addphotoannounce', [ "id" => $id ]);
        }

        return $this->render('Announce/create_photo.html.twig', [
            'createPhotoForm' => $form->createView(),
            'photos' => $photos
        ]);
    }

    /**
     * @Route("user/phone/{id}/delete")
     * @Route("/admin/phone/{id}/delete")
     */
    public function deleteAnnounceAction($id){
        //récupération du téléphone concerné
        $phone = $this->getDoctrine()
            ->getRepository(Phone::class)
            ->find($id);

        if($this->getUser()->isAdmin() or $phone->getUser()->getId() == $this->getUser()->getId()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($phone);
            $em->flush();

            return $this->redirectToRoute('app_announce_announcelist');
        }
        else{
            throw $this->createNotFoundException('Oops ! Page not found');
        }
    }

    /**
     * @Route("user/phone/{id}/photo/delete")
     * @Route("/admin/phone/{id}/photo/delete")
     */
    public function deletePhotoAnnounceAction($id){
        //récupération de la photo concernée
        $photo = $this->getDoctrine()
            ->getRepository(Photo::class)
            ->find($id);

        if($photo->getPhone()->getUser()->getId() == $this->getUser()->getId()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($photo);
            $em->flush();

            return $this->redirectToRoute('app_announce_addphotoannounce', [
                'id' => $photo->getPhone()->getId()
            ]);
        }
        else{
            throw $this->createNotFoundException('Oops ! Page not found');
        }
    }

    /**
     * @Route("/user/phone/{id}")
     * @Route("/admin/phone/{id}")
     */
    public function updateAnnounce($id, Request $request){

        //récupération du téléphone concerné
        $phone = $this->getDoctrine()
            ->getRepository(Phone::class)
            ->find($id);

        if(!$phone){
            throw $this->createNotFoundException('Oops ! Page not found');
        }

        if(!$this->getUser()->isAdmin() and $phone->getUser()->getId() != $this->getUser()->getId()){
            throw $this->createNotFoundException('Oops ! Page not found');
        }

        $form = $this->createForm(PhoneType::class, $phone);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $phone->setUpdated(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($phone);
            $em->flush();

            return $this->redirectToRoute('app_announce_updateannounce', [
                'id' => $phone->getId()
            ]);
        }

        return $this->render('User/create_phone.html.twig', [
            'phoneForm' => $form->createView(),
        ]);
    }
}