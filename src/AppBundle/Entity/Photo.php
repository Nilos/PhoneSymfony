<?php
/**
 * Created by PhpStorm.
 * User: Nil
 * Date: 27/11/2017
 * Time: 15:21
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository")
 * @ORM\Table(name="photo")
 */
class Photo
{
    use IdTrait;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $uri;

    /**
     * @ORM\ManyToOne(targetEntity="Phone", inversedBy="photos")
     * @JoinColumn(onDelete="CASCADE")
     */
    private $phone;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Photo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set uri
     *
     * @param string $uri
     *
     * @return Photo
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set phone
     *
     * @param \AppBundle\Entity\Phone $phone
     *
     * @return Photo
     */
    public function setPhone(\AppBundle\Entity\Phone $phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return \AppBundle\Entity\Phone
     */
    public function getPhone()
    {
        return $this->phone;
    }
}
