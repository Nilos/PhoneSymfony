<?php

/**
 * Created by PhpStorm.
 * User: Nil
 * Date: 27/11/2017
 * Time: 14:49
 */

namespace AppBundle\Entity;


trait IdTrait
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}