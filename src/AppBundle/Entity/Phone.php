<?php
/**
 * Created by PhpStorm.
 * User: Nil
 * Date: 27/11/2017
 * Time: 15:19
 */

namespace AppBundle\Entity;


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository")
 * @ORM\Table(name="phone")
 */
class Phone
{
    use IdTrait;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     */
    private $description;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Type(
     *     type="float",
     *     message="La valeur {{ value }} n'est pas une valeur possible pour ce champ."
     * )
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @Assert\Type(
     *     type="int",
     *     message="{{ value }} n'est pas une valeur possible pour ce champ."
     * )
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     */
    private $weight;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Type(
     *     type="int",
     *     message="{{ value }} n'est pas une valeur possible pour ce champ."
     * )
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     */
    private $warranty_year;

    /**
     * @ORM\Column(type="date")
     */
    private $created;

    /**
     * @ORM\Column(type="date")
     */
    private $updated;

    /**
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="phone")
     */
    private $photos;

    /**
     * @ORM\ManyToOne(targetEntity="Brand", inversedBy="phones")
     * @JoinColumn(onDelete="SET NULL")
     */
    private $brand;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="phones")
     * @JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Phone
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Phone
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Phone
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set weight
     *
     * @param string $weight
     *
     * @return Phone
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set warrantyYear
     *
     * @param string $warrantyYear
     *
     * @return Phone
     */
    public function setWarrantyYear($warrantyYear)
    {
        $this->warranty_year = $warrantyYear;

        return $this;
    }

    /**
     * Get warrantyYear
     *
     * @return string
     */
    public function getWarrantyYear()
    {
        return $this->warranty_year;
    }

    /**
     * Set created
     *
     * @param string $created
     *
     * @return Phone
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param string $updated
     *
     * @return Phone
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return string
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add photo
     *
     * @param \AppBundle\Entity\Photo $photo
     *
     * @return Phone
     */
    public function addPhoto(\AppBundle\Entity\Photo $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \AppBundle\Entity\Photo $photo
     */
    public function removePhoto(\AppBundle\Entity\Photo $photo)
    {
        $this->photos->removeElement($photo);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set brand
     *
     * @param \AppBundle\Entity\Brand $brand
     *
     * @return Phone
     */
    public function setBrand(\AppBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \AppBundle\Entity\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set User
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Phone
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get User
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
