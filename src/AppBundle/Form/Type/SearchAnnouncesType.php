<?php
/**
 * Created by PhpStorm.
 * User: Nil
 * Date: 04/12/2017
 * Time: 00:01
 */

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchAnnouncesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search', SearchType::class, ['label' => 'Rechercher']);
    }
}