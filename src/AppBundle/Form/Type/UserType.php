<?php

/**
 * Created by PhpStorm.
 * User: Nil
 * Date: 28/11/2017
 * Time: 13:35
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class UserType extends AbstractType
{

    private $authorizationChecker;

    public function __construct(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, ['label' => 'Email'])
            ->add('firstname', TextType::class, ['label' => 'Prénom'])
            ->add('lastname', TextType::class, ['label' => 'Nom'])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $user = $event->getData();

                if($user instanceof User){
                    if( null === $user->getId()){
                        $event->getForm()
                            ->add('password', RepeatedType::class, [
                                'type' => PasswordType::class,
                                'first_options' => ['label' => 'Mot de passe'],
                                'second_options' => ['label' => 'Répétez le mot de passe']
                            ]);
                    }

                    if($this->authorizationChecker->isGranted("ROLE_ADMIN")){
                        $event->getForm()
                            ->add('role', ChoiceType::class, array(
                                'choices'  => array(
                                    'Administrateur' => 'admin',
                                    'Utilisateur' => 'user',
                                )));
                    }
                }
            });
    }

}