<?php
/**
 * Created by PhpStorm.
 * User: Nil
 * Date: 28/11/2017
 * Time: 14:29
 */

namespace AppBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PhoneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Nom du téléphone'])
            ->add('description', TextType::class, ['label' => 'Description'])
            ->add('price', NumberType::class, ['label' => 'Prix'])
            ->add('weight', IntegerType::class, ['label' => 'Poids (gramme)'])
            ->add('warranty_year', IntegerType::class, ['label' => 'Garantie (année)'])
            ->add('brand', EntityType::class, ['label'=>'Marque' ,'class' => 'AppBundle:Brand']);
    }
}